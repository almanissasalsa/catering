-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2021 at 03:53 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arabella`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `nm_pelanggan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tgl_kirim` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `date`, `due_date`, `status`, `bukti`, `nm_pelanggan`, `alamat`, `nohp`, `email`, `tgl_kirim`) VALUES
(310, '2021-06-10 03:28:06', '2021-06-11 03:28:07', 'Unpaid', 'Belum ada', 'alma nissa', 'Komplek bukit Cimindi raya blok w no 10', '081909493850', 'almanissasalsa@gmail.com', '2021-02-12'),
(311, '2021-06-10 03:32:20', '2021-06-11 03:32:20', 'Unpaid', 'Belum ada', 'alma nissa', 'Komplek bukit Cimindi raya blok w no 10', '082909493850', 'deandra@gmail.com', '2021-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `gambar` varchar(100) NOT NULL,
  `nm_tanaman` varchar(50) NOT NULL,
  `deskripsi` varchar(250) NOT NULL,
  `harga` int(30) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`gambar`, `nm_tanaman`, `deskripsi`, `harga`, `kode`, `id`) VALUES
('Monstera-deliciosa.jpg', 'Monstera', 'Monstera merupakan jenis tanaman hias tropis yang tengah naik daun. Saat ini, monstera selalu dipakai sebagai dekorasi pelengkap pada beberapa konsep interior, seperti gaya minimalis dan Skandinavia. ', 200000, '1', 6),
('images.jpg', 'Janda Bolong ', 'Janda bolong merupakan anggota suku araceace, yang dimana daun pada tanaman ini adalah berlubang-lubang pada bagian tengah daun', 150000, '2', 7),
('283245bef52d9736707fbe7ee97091f09c26070e_01.jpg', 'Kuping Gajah ', 'tanaman kuping gajah adalah tanaman hias dari keluarga Araceae. tanaman ini memiliki manfaat menghilangkan stress, mengobati penyakit kulit dan masih banyak manfaat laiinya.', 60000, '3', 8),
('images_(6)1.jpg', 'Kaktus Coboy ', 'Kaktus koboi atau bernama latin cereus peruvianus merupakan jenis kaktus yang paling banyak diminati karena memiliki bentuk yang unik. Bentuknya simple, memajang vertikal dan ada juga yang memiliki cabang. Kaktus jenis ini cocok ditanam di tanah yang', 150000, '4', 9),
('screen-3.jpg', 'Kembang Sepatu ', 'Kemabang sepatu berasal dari Asia Timur dan banyak ditanam sebagai tanaman hias di daerah tropis dan subtropis. Bunga besar, berwarna merah dan tidak berbau. Bunga dari berbagai kultivar dan hibrida bisa berupa bunga tunggal (daun mahkota selapis) at', 35000, '5', 10),
('images_(5).jpg', 'Kuping Keledai ', 'Keladi atau Caladium merupakan tanaman hias yang cukup populer di Indonesia. Selain memiliki harga terjangkau, Keladi juga memiliki beragam corak unik yang cocok dijadikan hiasan di pekarangan rumah. Tak hanya corak, penamaan dari berbagai jenis Kela', 100000, '6', 11);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `qty` int(3) NOT NULL,
  `price` int(9) NOT NULL,
  `total` int(12) NOT NULL,
  `date` datetime NOT NULL,
  `nm_pelanggan` varchar(100) NOT NULL,
  `sub_total` int(20) NOT NULL,
  `tgl_kirim` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `invoice_id`, `product_id`, `product_name`, `qty`, `price`, `total`, `date`, `nm_pelanggan`, `sub_total`, `tgl_kirim`) VALUES
(318, 310, 6, 'Monstera', 1, 200000, 200000, '2021-06-10 03:28:07', 'alma nissa', 200000, '2021-02-12'),
(319, 311, 6, 'Monstera', 1, 200000, 260000, '2021-06-10 03:32:20', 'alma nissa', 200000, '2021-06-10'),
(320, 311, 8, 'Kuping Gajah ', 1, 60000, 260000, '2021-06-10 03:32:20', 'alma nissa', 60000, '2021-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin123', 'Admin'),
(2, 'Manager', 'manager', 'manager123', 'Pemilik'),
(3, 'keuangan', 'keuangan', 'keuangan123', 'Keuangan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`kode`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
